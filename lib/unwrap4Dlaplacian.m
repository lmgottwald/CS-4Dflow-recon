function [data_unwrapped,venc_new] = unwrap4Dlaplacian(data,venc)
% runs 4D laplacian unwrap by
% https://github.com/mloecher/4dflow-lapunwrap
%
%
% 15-Nov-2019 Lukas Gottwald (l.m.gottwald@amsterdamumc.nl)

%% 4D LAPLACIAN UNWRAP
input_angle = angle(squeeze(data));
input_abs = abs(squeeze(data));
dims = size(data);
nr_of_wraps = single(zeros(size(input_abs)));
output_angle = single(zeros(size(input_abs)));
if dims(10)==3 % is flow data set?
    for ii=1:dims(10)
        if length(size(input_angle(:,:,:,:,ii)))==4 % 4D data set?
            nr_of_wraps(:,:,:,:,ii) = unwrap_4D(input_angle(:,:,:,:,ii));
            output_angle(:,:,:,:,ii) = input_angle(:,:,:,:,ii) + 2*pi .* double(nr_of_wraps(:,:,:,:,ii));
        end
    end
end

%% RESCALE PHASE DATA
% find new maximal venc
magnitude  =   data_mask(abs(squeeze(data(:,:,:,1,1,:,1,1,1,1,1,1))),128);
velocities = angle(squeeze(data(:,:,:,1,1,:,1,1,1,:,1,1)));
temp = max(magnitude .*(sqrt(squeeze(velocities(:,:,:,:,1)).^2+squeeze(velocities(:,:,:,:,2)).^2+squeeze(velocities(:,:,:,:,3)).^2)),[],4); 
temp = data_rescale(temp); % scaling from 0 to 4096

% set PC-MRA intensity threshold
int_th = 900;
pcmra_mask = temp>int_th;
pcmra_mask = repmat(pcmra_mask,1,1,1,size(input_abs,4),size(input_abs,5));

% round new_venc to next 0 or 5
% check only the phase-wrapped voxel after masking with the thresholded PC-MRA
venc_new = ceil(max(abs(output_angle(pcmra_mask)))./pi*venc/5)*5;
venc_scaling = venc_new/venc;

output_angle = output_angle./venc_scaling;

% make complex output   
data_unwrapped = reshape(input_abs.*exp(output_angle*1i),dims);

end
function nr = unwrap_4D(phi_w, ts, real_flag)
%% UNWRAP_4D Unwraps a 4d array
%
% Args:
%     phi_w: Wrapped input array (-pi to pi) 
%     ts: Scales the temporal data to spatial dimensions
%     real_flag: restrcit laplacians to real (doesn't really matter, but
%     this lowers the memory load)
% 
% Returns:
%     nr: integer array containing the NUMBER of wraps per voxel
%         (note that this is not the actual unwrapped data)

if (nargin < 2), ts = 2; end
if (nargin < 3), real_flag = 1; end

[X Y Z T] = ndgrid(single(-size(phi_w,1)/2:size(phi_w,1)/2-1), ...
                   single(-size(phi_w,2)/2:size(phi_w,2)/2-1), ...
                   single(-size(phi_w,3)/2:size(phi_w,3)/2-1), ...
                   single(-size(phi_w,4)/2:size(phi_w,4)/2-1));
mod = 2.*cos(pi*X./size(phi_w,1)) + 2.*cos(pi*Y./size(phi_w,2)) + ...
      2.*cos(pi*Z./size(phi_w,3)) + ts.*cos(pi*T./size(phi_w,4))  - 6 - ts;

clear X Y Z T

lap_phiw = lap4(phi_w,1,mod,real_flag);
lap_phi = cos(phi_w).*lap4(sin(phi_w),1,mod,real_flag) - sin(phi_w).*lap4(cos(phi_w),1,mod,real_flag);
ilap_phidiff = (lap4(lap_phi-lap_phiw,-1,mod,real_flag));
nr = int8(round(ilap_phidiff./2./pi));
end
function out = lap4(in, dir, mod, real_flag)
%% LAP4 Runs 4D laplacian on input matrix
% 
% Args:
%     in: 3D input array
%     dir: forward or inverse transform (1 or -1)
%     mod: Laplaican kernel in frequency space
%     real_flag: restrict output to real (doesn't really matter, but this
%                lowers the memory load)
% 
% Returns:
%     out: output matrix


if (nargin < 4), real_flag = 0; end

[sx sy sz st] = size(in);

K = fftshift(fftn(ifftshift(in)));

if (dir==1)
    K = K.*mod;
elseif (dir==-1)
    mod(floor(sx/2)+1,floor(sy/2)+1,floor(sz/2)+1,floor(st/2)+1) = 1;
    K = K./mod;
else
    disp('ERROR')
end

if real_flag
    out = real(fftshift(ifftn(ifftshift(K))));
else
    out = fftshift(ifftn(ifftshift(K)));
end
end
function data = data_mask(data,NoiseClipValue)
% data = data_mask(data)
%
% mask the data based on a noise level

% take mean over entire data set (x,y,z, : )
dims = size(data);
th_data = mean(abs(reshape(data,[dims(1:3),prod(dims(4:end))])),4);

% make mask by threshold / NoiseClipValue
th_mask = th_data > NoiseClipValue;

% apply mask to data
th_mask_all = repmat(th_mask,[1,1,1,dims(4:end)]);
data = data .* th_mask_all;
end
function data = data_rescale(data)
% out = data_rescale(in)
% 
% scale data to 2^12 = 4096

datamax = ceil(max(abs(data(:))));
data = data ./ datamax .* 2^12;
end